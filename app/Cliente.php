<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Cliente;

class Cliente extends Model
{
    protected $fillable = [
        'id',
        'nombre',
        'apellidos',
        'cedula',
        'direccion',
        'telefono',
        'f_nacimiento',
        'email'
    ];
}
