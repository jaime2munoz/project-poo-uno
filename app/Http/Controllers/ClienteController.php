<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cliente;

class ClienteController extends Controller
{
    public function inicioCliente(Request $request)
    {
       $cliente = Cliente::all();
       return view('cliente.inicio')->with ('cliente', $cliente);
    }

    public function crearCliente(Request $request)
    {
        return view('cliente.crear')->with ('cliente', $cliente);
    }

    public function guardarCliente(Request $request)
    {
        $this->validate($request, [
            'nombre' => 'required',
            'apellidos' => 'required',
            'cedula' => 'required',
            'direccion' => 'required',
            'telefono' => 'required',
            'f_nacimiento' => 'required',
            'email' => 'required'
        ])
        $cliente = new Cliente;
        $cliente->nombre=$request->nombre;
        $cliente->apellidos=$request->apellidos;
        $cliente->cedula=$request->cedula;
        $cliente->direccion=$request->direccion;
        $cliente->telefono=$request->telefono;
        $cliente->f_nacimiento=$request->f_nacimiento;
        $cliente->email=$request->email;
        $cliente->save();
        return redirect()->route('list.clientes');
    }

    public function index()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
