@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">MODULO DE PRODUCTOS</div>

                <div class="card-body">
                    <table class="table">
                            <thead>
                                <tr>
                                <th scope="col"># ID</th>
                                <th scope="col">Nombre</th>
                                <th scope="col">Tipo</th>
                                <th scope="col">Estado</th>
                                <th scope="col">Precio</th>
                                </tr>
                            </thead>
                            <tbody>

                                @foreach($producto as $item){}
                                <tr>
                                <th scope="row">{{$item->id}}</th>
                                <td>{{$item->nombre}}</td>
                                <td>{{$item->tipo}}</td>
                                <td>{{$item->estado}}</td>
                                <td>{{$item->precio}}</td>
                                </tr>
                                @endforeach
                                <tr>
                                <th scope="row"></th>
                                <td></td>
                                <td></td>
                                <td></td>
                                </tr>
                                <tr>
                                <th scope="row"></th>
                                <td colspan="2"></td>
                                <td></td>
                                </tr>
                            </tbody>
                        </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
